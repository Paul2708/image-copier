import de.paul2708.copier.Main;
import de.paul2708.copier.model.ApplicationModel;

/**
 * This class stats the main class with different paths to test the view.
 *
 * @author Paul2708
 */
public class ViewTest {

    private static final String DEVICE_PATH = "src/test/resources/testphone";

    private static final String IMAGE_PATH = ViewTest.DEVICE_PATH + "/DCIM";

    private static final String TARGET_PATH = "target/image storage/mobile images";

    /**
     * Set the local paths and start the main method.
     *
     * @param args command line arguments
     * @see de.paul2708.copier.Main#main(String[])
     */
    public static void main(String[] args) {
        ApplicationModel model = ApplicationModel.getInstance();
        model.setPaths(ViewTest.DEVICE_PATH, ViewTest.IMAGE_PATH, ViewTest.TARGET_PATH);

        Main.main(args);

        // TODO: Clear files after moving
    }
}
