package de.paul2708.copier.controller;

import de.paul2708.copier.model.ApplicationModel;
import de.paul2708.copier.view.ApplicationFrame;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * This listener listens to the click event and copies the files.
 *
 * @author Paul2708
 */
public class CopyListener implements EventHandler<ActionEvent> {

    private ApplicationFrame frame;

    /**
     * Create a new copy listener with frame instance.
     *
     * @param frame application frame instance
     */
    public CopyListener(ApplicationFrame frame) {
        this.frame = frame;
    }

    /**
     * Copy the selected files.
     *
     * @param event the event which occurred
     */
    @Override
    public void handle(ActionEvent event) {
        ApplicationModel.getInstance().createOutputFile();
        ApplicationModel.getInstance().copy(frame.getDirectoryPane().getSelectedFiles());

        if (ApplicationModel.getInstance().hasCopied()) {
            frame.showMessage("Geschafft!", "Alle Bilder wurden kopiert.", false);
        }
    }
}
