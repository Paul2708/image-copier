package de.paul2708.copier.controller;

import de.paul2708.copier.model.ApplicationModel;
import de.paul2708.copier.view.ApplicationFrame;
import java.io.File;
import java.util.List;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * This listener listens to the click event and searches for the device.
 *
 * @author Paul2708
 */
public class SearchListener implements EventHandler<ActionEvent> {

    private ApplicationFrame frame;

    /**
     * Create a new search listener with frame instance.
     *
     * @param frame application frame instance
     */
    public SearchListener(ApplicationFrame frame) {
        this.frame = frame;
    }

    /**
     * Search for the device.
     *
     * @param event the event which occurred
     */
    @Override
    public void handle(ActionEvent event) {
        ApplicationModel.getInstance().searchDevice();

        if (ApplicationModel.getInstance().isDevicePluggedIn()) {
            ApplicationModel.getInstance().searchDirectories();

            List<File> files = ApplicationModel.getInstance().getDirectories();

            if (files == null || files.isEmpty()) {
                frame.showMessage("Achtung!", "Es wurden keine Alben gefunden.", true);
                return;
            }

            frame.switchScene(ApplicationModel.getInstance().getDirectories());
        }
    }
}
