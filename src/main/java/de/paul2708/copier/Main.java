package de.paul2708.copier;

import de.paul2708.copier.view.ApplicationFrame;

/**
 * This is the main class and starts the program.
 *
 * @author Paul2708
 */
public final class Main {

    /**
     * Nothing to call here..
     */
    private Main() {
        throw new IllegalAccessError();
    }

    /**
     * The main method and program entry.
     *
     * @param args command line arguments
     */
    public static void main(String[] args) {
        new Thread(() -> ApplicationFrame.launch(ApplicationFrame.class)).start();
    }
}
