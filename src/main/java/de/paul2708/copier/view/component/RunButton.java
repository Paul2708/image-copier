package de.paul2708.copier.view.component;

import de.paul2708.copier.controller.CopyListener;
import de.paul2708.copier.controller.SearchListener;
import de.paul2708.copier.model.ApplicationModel;
import de.paul2708.copier.model.observer.Observer;
import de.paul2708.copier.view.ApplicationFrame;
import javafx.scene.control.Button;

/**
 * This class holds the search and confirm button and is an observer.
 *
 * @author Paul2708
 */
public class RunButton implements Observer<ApplicationModel> {

    private Button button;
    private ApplicationFrame frame;

    /**
     * Create a new run button by referring to a {@link javafx.scene.control.Button}.
     *
     * @param button javafx button
     * @param frame application frame instance
     */
    public RunButton(Button button, ApplicationFrame frame) {
        this.button = button;
        this.frame = frame;

        button.setOnAction(new SearchListener(frame));

        ApplicationModel.getInstance().registerObserver(this);
    }

    /**
     * Invoke an update on a subject.
     *
     * @param applicationModel called subject
     */
    @Override
    public void invokeUpdate(ApplicationModel applicationModel) {
        if (applicationModel.getDirectories() != null && !applicationModel.getDirectories().isEmpty()) {
            button.setText("Bestätigen.");

            button.setOnAction(new CopyListener(frame));
        }

        button.setDisable(applicationModel.isRunning());
    }
}
