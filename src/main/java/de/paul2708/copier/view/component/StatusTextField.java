package de.paul2708.copier.view.component;

import de.paul2708.copier.model.ApplicationModel;
import de.paul2708.copier.model.observer.Observer;
import javafx.scene.control.TextField;

/**
 * This class holds the status label and is an observer.
 *
 * @author Paul2708
 */
public class StatusTextField implements Observer<ApplicationModel> {

    private TextField textField;

    /**
     * Create a new status text field, set default status and register the observer.
     *
     * @param textField javafx text field component
     */
    public StatusTextField(TextField textField) {
        this.textField = textField;

        this.textField.setText(ApplicationModel.getInstance().getStatus());

        ApplicationModel.getInstance().registerObserver(this);
    }

    /**
     * Invoke an update on a subject.
     *
     * @param applicationModel called subject
     */
    @Override
    public void invokeUpdate(ApplicationModel applicationModel) {
        textField.setText(applicationModel.getStatus());
    }
}
