package de.paul2708.copier.view.component;

import de.paul2708.copier.model.ApplicationModel;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * This class holds the directory pane and adds the image directories to list.
 *
 * @author Paul2708
 */
public class DirectoryPane {

    private List<File> files;

    private Map<CheckBox, File> fileMap;

    /**
     * Create a new directory pane with a list of files.
     *
     * @param files list that will be displayed
     */
    public DirectoryPane(List<File> files) {
        this.files = files;

        this.fileMap = new HashMap<>();
    }

    /**
     * Get the pane by loading it.
     *
     * @return directory pane
     */
    public Parent build() {
        try {
            return FXMLLoader.load(Objects.requireNonNull(
                    getClass().getClassLoader().getResource("view/directory_pane.fxml")));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Set the list to the box.
     *
     * @param box box
     */
    public void setFiles(VBox box) {
        if (files.size() == 0) {
            box.getChildren().add(new Label("Keine Alben gefunden"));
            return;
        }

        for (File file : files) {
            String title = file.getName() + " (" + file.listFiles().length + " Bilder)";
            CheckBox checkbox = new CheckBox(title);

            box.getChildren().add(checkbox);
            fileMap.put(checkbox, file);
        }
    }

    /**
     * Set the text field's text to the output path.
     *
     * @param textField text field
     */
    public void setOutputPath(TextField textField) {
        textField.setText(ApplicationModel.getInstance().getOutputPath());
    }

    /**
     * Get a list of all selected files.
     *
     * @return list of selected files
     */
    public List<File> getSelectedFiles() {
        List<File> selectedFiles = new LinkedList<>();

        for (Map.Entry<CheckBox, File> entry : fileMap.entrySet()) {
            if (entry.getKey().isSelected()) {
                selectedFiles.add(entry.getValue());
            }
        }

        return selectedFiles;
    }
}
