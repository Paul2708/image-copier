package de.paul2708.copier.view;

import de.paul2708.copier.model.ApplicationModel;
import de.paul2708.copier.view.component.DirectoryPane;
import de.paul2708.copier.view.component.RunButton;
import de.paul2708.copier.view.component.StatusTextField;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Objects;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * This class holds the main frame and is the view part of MVC.
 *
 * @author Paul2708
 */
public class ApplicationFrame extends Application {

    private static final int WIDTH = 600;
    private static final int HEIGHT = 400;
    private static final String TITLE = "Image-Transfer v1.0";
    private static final URL ICON_URL = ApplicationFrame.class.getClassLoader().getResource("icon/copy.png");

    private ApplicationModel model;

    private StatusTextField textField;
    private RunButton button;
    private DirectoryPane directoryPane;

    private Scene scene;

    /**
     * Create a new application frame. Note: Do not pass parameters as
     * {@link javafx.application.Application#launch(Class, String...)} is creating a new default instance.
     */
    public ApplicationFrame() {
        this.model = ApplicationModel.getInstance();
    }

    /**
     * Inherited start method from {@link javafx.application.Application}.
     *
     * @param primaryStage primary stage
     * @throws Exception if an error occurs while loading the stage
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(Objects.requireNonNull(
                getClass().getClassLoader().getResource("view/frame.fxml")));

        this.scene = new Scene(root, ApplicationFrame.WIDTH, ApplicationFrame.HEIGHT);

        primaryStage.setTitle(ApplicationFrame.TITLE);

        // Set icon
        primaryStage.getIcons().add(new Image(ApplicationFrame.ICON_URL.toString()));

        primaryStage.setScene(scene);

        initializeComponents(scene);

        primaryStage.show();
    }

    /**
     * Switch the scene to the directory pane and set the files.
     *
     * @param files files to set
     * @see de.paul2708.copier.view.component.DirectoryPane
     */
    public void switchScene(List<File> files) {
        this.directoryPane = new DirectoryPane(files);
        Parent pane = directoryPane.build();

        BorderPane borderPane = (BorderPane) scene.lookup("#border");
        borderPane.setCenter(pane);
        BorderPane.setAlignment(pane, Pos.CENTER_RIGHT);

        // Get vbox
        SplitPane pane1 = (SplitPane) borderPane.getCenter();
        BorderPane pane2 = (BorderPane) pane1.getItems().get(0);
        ScrollPane pane3 = (ScrollPane) pane2.getCenter();
        VBox box = (VBox) pane3.getContent();

        // Get text field
        pane2 = (BorderPane) pane1.getItems().get(1);
        FlowPane flowPane = (FlowPane) pane2.getCenter();
        TextField field = (TextField) flowPane.getChildren().get(0);

        directoryPane.setOutputPath(field);
        directoryPane.setFiles(box);
    }

    /**
     * Show a message alert with header and content.
     *
     * @param header header text
     * @param description content text
     * @param error error message or not
     */
    public void showMessage(String header, String description, boolean error) {
        Alert alert = new Alert(error ? Alert.AlertType.ERROR : Alert.AlertType.INFORMATION);

        alert.setHeaderText(header);
        alert.setContentText(description);

        // Set icon
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(ApplicationFrame.ICON_URL.toString()));

        alert.showAndWait();
    }

    /**
     * Initialize required components in the given scene.
     *
     * @param scene scene
     * @see #start(javafx.stage.Stage)
     */
    private void initializeComponents(Scene scene) {
        TextField textField = (TextField) scene.lookup("#status");
        this.textField = new StatusTextField(textField);

        Button button = (Button) scene.lookup("#button");
        this.button = new RunButton(button, this);
    }

    /**
     * Get the directory pane.
     *
     * @return directory pane or <code>null</code> if the scene wasn't switched
     * @see #switchScene(java.util.List)
     */
    public DirectoryPane getDirectoryPane() {
        return directoryPane;
    }
}
