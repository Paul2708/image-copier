package de.paul2708.copier.model.observer;

/**
 * This interface is an observer and updates the subject.
 *
 * @param <Subject> subject to update
 * @author Paul2708
 */
public interface Observer<Subject> {

    /**
     * Invoke an update on a subject.
     *
     * @param subject called subject
     */
    void invokeUpdate(Subject subject);
}
