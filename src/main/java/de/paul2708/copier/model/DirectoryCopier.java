package de.paul2708.copier.model;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * This file visitor visits all subfiles and copies them.
 *
 * @author Paul2708
 */
public class DirectoryCopier implements FileVisitor<Path> {

    private Path source;
    private Path target;

    /**
     * Create a new directory copier.
     *
     * @param source source directory
     * @param target target directory
     */
    public DirectoryCopier(Path source, Path target) {
        this.source = source;
        this.target = target;
    }

    /**
     * Copy the source file to the target file by walking through the file tree.
     *
     * @throws IOException if an exception occurs while coping
     */
    public void copy() throws IOException {
        Files.walkFileTree(source, this);
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
        Path newTarget = target.resolve(source.relativize(dir));

        try {
            Files.copy(dir, newTarget);
        } catch (IOException e) {
            // Ignore FileAlreadyExistsException here
            // e.printStackTrace();
        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        try {
            Files.copy(file, target.resolve(source.relativize(file)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        return FileVisitResult.CONTINUE;
    }
}
