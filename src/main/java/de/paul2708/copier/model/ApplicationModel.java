package de.paul2708.copier.model;

import de.paul2708.copier.model.observer.Observer;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * This class represents the model in the MVC and copies the images. The class is a singleton.
 *
 * @author Paul2708
 */
public final class ApplicationModel {

    private static final ApplicationModel INSTANCE = new ApplicationModel();

    private static final String DEVICE_PATH = "Z:/";

    private static final String IMAGE_PATH = ApplicationModel.DEVICE_PATH + "/DCIM";
    // TODO: Set target path
    private static final String TARGET_PATH = "C:/Users/Bianca/Pictures/Handyfotos";

    private String devicePath;
    private String imagePath;
    private String targetPath;

    private Set<Observer<ApplicationModel>> observers;

    private String status;
    private boolean running;

    private boolean foundDevice;
    private List<File> directories;
    private String ouputPath;
    private boolean copied;

    /**
     * Create a new application with an empty list of observers and standard values.
     */
    private ApplicationModel() {
        setPaths(DEVICE_PATH, IMAGE_PATH, TARGET_PATH);

        this.observers = new HashSet<>();

        setStatus("nicht gestartet");
        setRunning(false);

        this.foundDevice = false;
    }

    /**
     * Set the device, image and target path. Used to test the singleton class.
     *
     * @param device device path
     * @param image image path
     * @param target target path
     */
    public void setPaths(String device, String image, String target) {
        this.devicePath = device;
        this.imagePath = image;
        this.targetPath = target;
    }

    /**
     * Search for the device and change status and running.
     */
    public void searchDevice() {
        setRunning(true);
        setStatus("Device suchen..");

        Path path = Paths.get(this.devicePath);
        this.foundDevice = path != null && Files.exists(path);

        setStatus("Device " + (foundDevice ? "gefunden" : "nicht gefunden"));
        setRunning(false);
    }

    /**
     * Check if the device was found or not
     *
     * @return true if the device was found, otherwise false
     * @see #searchDevice()
     */
    public boolean isDevicePluggedIn() {
        return foundDevice;
    }

    /**
     * Search the image directories.
     */
    public void searchDirectories() {
        setRunning(true);
        setStatus("Alben suchen..");

        this.directories = new LinkedList<>();

        Path path = Paths.get(this.imagePath);
        if (path != null && Files.exists(path)) {
            File file = path.toFile();
            for (File subFile : file.listFiles()) {
                if (subFile != null && subFile.isDirectory()) {
                    directories.add(subFile);
                }
            }
        }

        setStatus(directories.isEmpty() ? "keine Alben gefunden" : directories.size() + " Alben gefunden");
        setRunning(false);
    }

    /**
     * Get a list of image directories.
     *
     * @return list of directories, or an empty list if the path doesn't exist
     * @see #searchDirectories()
     */
    public List<File> getDirectories() {
        return directories;
    }

    /**
     * Get the current output path or create it.
     *
     * @return output path
     */
    public String getOutputPath() {
        if (this.ouputPath == null) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.M.yyyy (hh.mm.ss)");
            this.ouputPath = targetPath + "/" + dateFormat.format(new Date());
        }


        return ouputPath;
    }

    /**
     * Create the output directory, where the files are copied to.
     */
    public void createOutputFile() {
        setRunning(true);
        setStatus("Verzeichnis wird erstellt..");

        try {
            Files.createDirectories(Paths.get(getOutputPath()));

            setStatus("Verzeichnis erstellt");
        } catch (IOException e) {
            e.printStackTrace();
            setStatus("Fehler beim Erstellen");
        }

        setRunning(false);
    }

    /**
     * Copy the selected files to the output directory.
     *
     * @param files selected files
     * @see #createOutputFile()
     */
    public void copy(List<File> files) {
        setRunning(true);
        setStatus("Bilder werden kopiert..");

        for (File file : files) {
            DirectoryCopier copier = new DirectoryCopier(file.toPath(),
                    Paths.get(getOutputPath() + "/" + file.getName()));

            try {
                copier.copy();
            } catch (IOException e) {
                e.printStackTrace();
                setStatus("Fehler ist aufgetreten");
                this.copied = false;

                return;
            }
        }

        this.copied = true;

        setStatus("Bilder wurden kopiert");
        setRunning(false);
    }

    /**
     * Check if the files have been copied.
     *
     * @return true if the files have been copied, otherwise false
     */
    public boolean hasCopied() {
        return copied;
    }

    /**
     * Set the model as running and inform the observers. For example: Searching or Coping.
     *
     * @param running new running value
     */
    public void setRunning(boolean running) {
        this.running = running;

        informObservers();
    }

    /**
     * Check if the model is running (coping, searching,..) or not.
     *
     * @return true, if the model is running, otherwise false
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * Set the current status and inform all observers.
     *
     * @param status new status
     */
    public void setStatus(String status) {
        this.status = status;

        informObservers();
    }

    /**
     * Get the current status of the model.
     *
     * @return status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Register an observer to this application.
     *
     * @param observer registered observer
     */
    public void registerObserver(Observer<ApplicationModel> observer) {
        this.observers.add(observer);
    }

    /**
     * Inform all registered observers.
     *
     * @see #registerObserver(de.paul2708.copier.model.observer.Observer)
     */
    private void informObservers() {
        for (Observer<ApplicationModel> observer : observers) {
            observer.invokeUpdate(this);
        }
    }

    /**
     * Get the instance of the model as it is a singleton.
     *
     * @return model instance
     */
    public static ApplicationModel getInstance() {
        return ApplicationModel.INSTANCE;
    }
}
