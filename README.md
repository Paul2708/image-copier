# Image Copier
---
This small application copies images and files from a device to a specific path.

## Idea
I created that application to support my mother as she is not into the "computer stuff".
Unfortunately, I didn't know, that a wearable device is not as handled as a usb storage on Windows.
So I cannot get any path to the device. Means: This application is useless.

## How to use
### Run demo
You can run the `ViewTest`-Class to test the view.

* `git clone https://Paul2708@bitbucket.org/Paul2708/image-copier.git`
* Run `ViewTest` by your IDE.

### Run main application
You can run the main application, but the paths are constant.

* `git clone https://Paul2708@bitbucket.org/Paul2708/image-copier.git`
* `mvn clean package`
* Simple execute the jar by double-click or `java -jar image-copier`

## Development
As I can't access the device, I won't enhance this application anymore. (e.g. writing tests)
I used to apply some design patterns I learned: MVC as architecture and the observer.